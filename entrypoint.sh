#!/bin/bash

set -e

# update config
envtpl < /app/thumbor.conf.tpl > /etc/thumbor.conf

# run
exec thumbor